package app;

/**
 *  Made by Thitiwut Chutipongqwanit 602115012
 */
public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Visitor Demo");

        ObjectStructure o = new ObjectStructure();
        o.Attach(new ElementA());
        o.Attach(new ElementB());

        Visitor1 v1 = new Visitor1();
        Visitor2 v2 = new Visitor2();
        Visitor3 v3 = new Visitor3();
        Visitor4 v4 = new Visitor4();
        Visitor5 v5 = new Visitor5();

        o.Accept(v1);
        o.Accept(v2);
        o.Accept(v3);
        o.Accept(v4);
        o.Accept(v5);
    }
}