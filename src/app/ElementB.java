package app;

public class ElementB extends Element{

    @Override
    public void Accept(Visitor v) {
       v.VisitElementB(this);
    }

    public void OperationB(){}

}