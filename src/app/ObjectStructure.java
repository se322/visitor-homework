package app;

import java.util.ArrayList;
import java.util.List;

public class ObjectStructure{
    private List<Element> elements = new ArrayList<Element>();

    public void Attach(Element e){
        elements.add(e);
    }

    public void Detach(Element e){
        elements.remove(e);
    }

    public void Accept(Visitor v){
        for(Element e : elements){
            e.Accept(v);
        }
    }
}