package app;

public abstract class Visitor{
    public abstract void VisitElementA(ElementA A);
    public abstract void VisitElementB(ElementB B);
}