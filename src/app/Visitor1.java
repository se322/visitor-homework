package app;

public class Visitor1 extends Visitor{

    @Override
    public void VisitElementA(ElementA A) {
        System.out.println(A.getClass().getName() + " visited by " + this.getClass().getName());

    }

    @Override
    public void VisitElementB(ElementB B) {
        System.out.println(B.getClass().getName() + " visited by " + this.getClass().getName());
    }

}