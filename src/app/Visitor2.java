package app;

public class Visitor2 extends Visitor{

    @Override
    public void VisitElementA(ElementA A) {
        System.out.println(A.getClass().getName() + " visited by " + this.getClass().getName());

    }

    @Override
    public void VisitElementB(ElementB B) {
        System.out.println(B.getClass().getName() + " visited by " + this.getClass().getName());
    }

}