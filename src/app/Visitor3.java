package app;

public class Visitor3 extends Visitor {
    @Override
    public void VisitElementA(ElementA A) {
        System.out.println(this.getClass().getName() + " Do operation A to " + A.getClass().getName());
    }

    @Override
    public void VisitElementB(ElementB B) {
        System.out.println(this.getClass().getName() + " Do operation A to " + B.getClass().getName());
    }
}
