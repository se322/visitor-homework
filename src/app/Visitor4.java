package app;

public class Visitor4 extends Visitor {
    @Override
    public void VisitElementA(ElementA A) {
        System.out.println(this.getClass().getName() + " Do operation B to " + A.getClass().getName());
    }

    @Override
    public void VisitElementB(ElementB B) {
        System.out.println(this.getClass().getName() + " Do operation B to " + B.getClass().getName());
    }
}
