package app;

public class Visitor5 extends Visitor {
    @Override
    public void VisitElementA(ElementA A) {
        System.out.println(this.getClass().getName() + " Do operation C to " + A.getClass().getName());
    }

    @Override
    public void VisitElementB(ElementB B) {
        System.out.println(this.getClass().getName() + " Do operation C to " + B.getClass().getName());
    }
}
